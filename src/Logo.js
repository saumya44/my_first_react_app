import React from "react";
import Container from "react-bootstrap/Container";
import Navbar from "react-bootstrap/Navbar";
// import grooot from './grooot.png';
import "./App.css";

const Logo = (props) => {
  return (
    <>
      <Navbar bg="light" variant="light">
        <Container>
          <Navbar.Brand href="#home">{props.name}</Navbar.Brand>
          <img
            src={props.src}
            alt="Girl in a jacket"
            width="50"
            height="50"
          ></img>
        </Container>
      </Navbar>
    </>
  );
};

export default Logo;
