import React from "react";
import Header from "./Header";
import Click from "./Click";
import Home from "./Home";
import Class_comp from "./Class_comp";
import SwiperComp from "./Sliders/SwiperComp";
import SwiperComp2 from "./SwiperComp2";
import { LinkContainer } from "react-router-bootstrap";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Footer from "./Footer";

import About from "./About";
import Contact from "./Contact";
const App = () => {
  return (
    <div>
      <Router>
        <Header />
        <Routes>
          <Route path="/about" element={<About />}></Route>
          <Route path="/contact" element={<Contact />}></Route>
          <Route path="/" element={<Home />}></Route>
        </Routes>
      </Router>
      <br></br>
      <SwiperComp />
      <br></br>
      <SwiperComp2 />

      <br></br>
      <br></br>
      <Click />
      <Class_comp />
      <br></br>
      <br></br>
      <Footer />
    </div>
  );
};

export default App;
