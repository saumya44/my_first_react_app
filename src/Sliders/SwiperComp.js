import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";
import img1 from "../img1.jpg";
import img2 from "../img2.jpg";
import img3 from "../img3.jpg";
import img4 from "../img4.jpg";
import img5 from "../img5.jpg";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

// import "./styles.css";

// import required modules
import { Pagination, Navigation, HashNavigation } from "swiper";

const SwiperComp = () => {
  return (
    <div>
      <Swiper
        spaceBetween={30}
        hashNavigation={{
          watchState: true,
        }}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Pagination, Navigation, HashNavigation]}
        className="mySwiper"
      >
        <SwiperSlide>
          <img src={img1} alt="Italian Trulli" width="1500" height="500"></img>
        </SwiperSlide>
        <SwiperSlide>
          <img src={img2} alt="Italian Trulli" width="1500" height="500"></img>
        </SwiperSlide>
        <SwiperSlide>
          <img src={img3} alt="Italian Trulli" width="1500" height="500"></img>
        </SwiperSlide>
        <SwiperSlide>
          <img src={img4} alt="Italian Trulli" width="1500" height="500"></img>
        </SwiperSlide>
        <SwiperSlide>
          <img src={img5} alt="Italian Trulli" width="1500" height="500"></img>
        </SwiperSlide>
      </Swiper>
    </div>
  );
};

export default SwiperComp;
