import React from "react";

class Class_comp extends React.Component {
  constructor(props) {
    super(props);
    this.state = { change: true };
  }
  render() {
    return (
      <div>
        <td>&nbsp;</td>

        {this.state.change ? (
          <h5>Hi how are you?</h5>
        ) : (
          <h5>Hope you are good :)</h5>
        )}
        <button
          onClick={() => {
            this.setState({ change: !this.state.change });
          }}
        >
          {" "}
          Click me!
        </button>
      </div>
    );
  }
}

export default Class_comp;
