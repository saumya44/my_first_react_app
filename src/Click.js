import React, { useEffect, useState } from "react";

const Click = () => {
  const [count, setCount] = useState(1);
  useEffect(() => {
    console.warn(count);
  }, [count === 5]);

  return (
    <div>
      <h5>Now the count is .....{count}</h5>

      <tr>
        <button onClick={() => setCount(count + 1)}>increase</button>
        <td>&nbsp;&nbsp;</td>
        <button onClick={() => setCount(count - 1)}>decrease</button>
        <br></br>
      </tr>
    </div>
  );
};

export default Click;
