//import React from "react";
import React, { useRef, useState } from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

import img6 from "./img6.jpg";

import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";
import "swiper/css/free-mode";
import "swiper/css/scrollbar";
import "swiper/css/effect-coverflow";
import { EffectCoverflow, FreeMode, Scrollbar, Mousewheel } from "swiper";

import { Pagination, Navigation, HashNavigation } from "swiper";

export default function SwiperComp2() {
  return (
    <>
      <div style={{ width: "50%", display: "inline-block" }}>
        <Swiper
          effect={"coverflow"}
          grabCursor={true}
          centeredSlides={true}
          slidesPerView={"auto"}
          coverflowEffect={{
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
          }}
          // pagination={true}
          modules={[EffectCoverflow, Pagination]}
          className="mySwiper"
        ></Swiper>
        <SwiperSlide>
          {/* <img src={img6} width="700" /> */}
          <img src={img6} width="700" height="400"></img>
        </SwiperSlide>
      </div>
      <div style={{ width: "50%", display: "inline-block" }}>
        <Swiper
          direction={"vertical"}
          slidesPerView={"auto"}
          freeMode={true}
          scrollbar={true}
          mousewheel={true}
          coverflowEffect={{
            rotate: 50,
            stretch: 0,
            depth: 100,
            modifier: 1,
            slideShadows: true,
          }}
          modules={[FreeMode, Scrollbar, Mousewheel]}
          className="SwiperComp2"
        >
          <SwiperSlide>
            {/* <body style="background-color:rgba(89, 0, 255, 0.334)"> */}
            <h4>Caramel Frappuccino</h4>
            <p>
              Coffee blended with caramel syrup, milk and ice. Topped with
              whipped cream and caramel sauce. <br></br>
              Starbucks caramel drinks are the best, and the caramel frappuccino
              has been one of the <br></br>
              most popular since its introduction. In fact, Starbucks’ website
              tells us that it’s “hands down” the most popular.<br></br>
              You just have to drink one and it’s easy to understand why!
            </p>
            {/* </body> */}
          </SwiperSlide>
        </Swiper>
      </div>
    </>
  );
}
