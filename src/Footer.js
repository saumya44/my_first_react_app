import React from "react";
import { MDBFooter } from "mdb-react-ui-kit";
import {
  AiOutlineSend,
  AiFillYoutube,
  AiFillInstagram,
  AiFillFacebook,
  AiFillTwitterSquare,
} from "react-icons/ai";

const stylecss = {
  color: "black",
};

export default function App() {
  return (
    <MDBFooter bgColor="light" className="text-center text-lg-left">
      <div
        className="text-center p-3"
        style={{ backgroundColor: "rgba(0, 0, 0, 0.2)" }}
      >
        &copy; {new Date().getFullYear()} Copyright:{" "}
        <a className="text-dark">starbucks.com</a>
        <br></br>
        <a href="https://www.instagram.com/starbucks/?hl=en">
          <AiFillInstagram style={stylecss} size={20} />
        </a>
        <a href="https://www.youtube.com/@starbucks">
          <AiFillYoutube style={stylecss} size={20} />
        </a>
        <a href="https://www.facebook.com/Starbucks/">
          <AiFillFacebook style={stylecss} size={20} />
        </a>
        <a href="https://twitter.com/starbucksindia">
          <AiFillTwitterSquare style={stylecss} size={20} />
        </a>
      </div>
    </MDBFooter>
  );
}
