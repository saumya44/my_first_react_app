import React from "react";

const Contact = () => {
  return (
    <div className="container">
      Reach us at Tata Starbucks Private Limited <br></br>
      CIN: U74900MH2011PTC222589 <br></br>
      4th Floor, New Excelsior Building, Amrit Keshav Nayak Marg, Fort, Mumbai
      400001 <br></br>
      Ph No. 022-66113939, email: contact@tatastarbucks.com <br></br>
    </div>
  );
};

export default Contact;
