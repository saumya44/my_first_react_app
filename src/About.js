import React from "react";

const About = () => {
  return (
    <div className="container">
      It happens millions of times each week a customer receives a drink from a
      Starbucks barista but each interaction is unique. It’s just a moment in
      time just one hand reaching over the counter to present a cup to another
      outstretched hand. But it’s a connection.... We make sure everything we do
      honors that connection from our commitment to the highest quality coffee
      in the world, to the way we engage with our customers and communities to
      do business responsibly. From our beginnings as a single store over forty
      years ago, in every place that we’ve been, and every place that we touch,
      we've tried to make it a little better than we found it.
    </div>
  );
};

export default About;
